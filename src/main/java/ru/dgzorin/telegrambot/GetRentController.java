package ru.dgzorin.telegrambot;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import ru.dgzorin.database.Database;

public class GetRentController {

	boolean checkApartamentList(Message message, String tableName, String district) {
		return Database.checkApartamentList(tableName, district);
	}

	ArrayList<SendPhoto> sendApartamentList(Message message, String tableName, String district) {
		ArrayList<SendPhoto> apartamentList = new ArrayList<>();
		try {

			ResultSet rs = Database.getApartamentList(tableName, district);

			while (rs.next()) {

				String caption = "";
				String[] photoName = rs.getString(2).split(Constants.photosSplitMarker);

				caption += "Метро " + rs.getString(3) + "\n" + rs.getString(4) + "\n" + rs.getString(5);

				SendPhoto sendPhotoRequest = new SendPhoto();
				File file;

				InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
				List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
				List<InlineKeyboardButton> rowInline = new ArrayList<>();
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					file = new File("photos/missing_foto.jpg");
				} else {
					file = new File("photos/get_rent/" + rs.getString(6) + "/" + photoName[0] + ".jpg");
				}

				rowInline.add(
						new InlineKeyboardButton().setText("Подробнее").setCallbackData(String.valueOf(rs.getInt(1))));

				rowsInline.add(rowInline);
				markupInline.setKeyboard(rowsInline);

				sendPhotoRequest.setChatId(message.getChatId().toString());

				sendPhotoRequest.setNewPhoto(file);
				sendPhotoRequest.setCaption(caption);
				sendPhotoRequest.setReplyMarkup(markupInline);

				apartamentList.add(sendPhotoRequest);
			}
			rs.close();
			return apartamentList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	ArrayList<SendPhoto> sendSelectedApartamentPhotos(String chatId, String tableName, String id) {
		ArrayList<SendPhoto> photoNamesList = new ArrayList<>();

		try {
			ResultSet rs = Database.getSelectedApartamentPhotos(tableName, id);
			String[] photoNames = rs.getString(1).split(Constants.photosSplitMarker);

			for (String tempName : photoNames) {
				if (!(tempName == null) && !tempName.equals("")) {
					SendPhoto sendPhotoRequest = new SendPhoto();
					File file;
					file = new File("photos/get_rent/" + rs.getString(2) + "/" + tempName + ".jpg");
					sendPhotoRequest.setChatId(chatId);
					sendPhotoRequest.setNewPhoto(file);
					photoNamesList.add(sendPhotoRequest);
				}
			}
			rs.close();
			return photoNamesList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	SendMessage sendSelectedApartamentData(String chatId, ArrayList<KeyboardRow> keyboard, String tableName,
			String id) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		String caption = "";

		try {
			ResultSet rs = Database.getSelectedApartamentData(tableName, id);

			caption += "Метро " + rs.getString(1) + "\n" + rs.getString(2) + "\n" + rs.getString(4) + " район\n"
					+ rs.getString(3) + "\n" + rs.getString(5);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		sendMessage.setChatId(chatId);
		sendMessage.setText(caption);
		return sendMessage;
	}

	SendMessage sendRenterData(SendMessage sendMessage, String chatId, ArrayList<KeyboardRow> keyboard,
			String tableName, String id) {
		sendMessage.enableMarkdown(true);

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		String caption = "";

		try {
			ResultSet rs = Database.getRenterData(tableName, id);
			caption += "Арендатор : " + rs.getString(1) + "\nТелефон : " + rs.getString(2);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		sendMessage.setChatId(chatId);
		sendMessage.setText(caption);

		return sendMessage;
	}

	String getFinishOrder(String chatId, String clientName1, String phoneNumer, String tableName, String id) {
		SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy 'at' HH.mm ':'");

		String order = "" + date.format(new Date()) + Constants.newOrderFromTelegram+"\nСНЯТЬ !" + "\nКлиент : " + chatId
				+ "\n Имя : " + clientName1 + "\nТелефон : " + phoneNumer + "\n";

		try {
			ResultSet rs = Database.getSelectedApartamentData(tableName, id);
			ResultSet rs2 = Database.getRenterData(tableName, id);

			order += "\nАрендодатель : " + rs2.getString(1) + "\nИмя : " + rs2.getString(2) + "\nТелефон : "
					+ rs2.getString(3) + "\n";

			String type = "";

			switch (tableName) {
			case "room1":
				type = "1-комнатная";
				break;
			case "room2":
				type = "2-комнатная";

				break;
			case "room3":
				type = "3-комнатная";

				break;
			case "room4":
				type = "4-комнатная";

				break;
			case "roomOnly":
				type = "Комната";

				break;
			case "studio":
				type = "Студия";
				break;
			default:
				break;
			}

			order += "\nЖелаемая квартира :\nМетро " + rs.getString(1) + "\n" + rs.getString(2) + "\n" + rs.getString(4)
					+ " район\n" + "Тип : " + type + "\n" + rs.getString(3) + "\n" + rs.getString(5);

		} catch (SQLException e) {
			e.printStackTrace();
			return "Error !";
		}
		return order;
	}
}
