package ru.dgzorin.telegrambot;

public class Constants {

	static final String chooseSection = "Выберите интересующий Вас раздел.";
	static final String chooseDistrict = "Выберите подходящий Вам район.";
	static final String startMessage = "Приветствуем Вас в нашем сервисе по подбору недвижимости в аренду!";
	static final String startMessage2 = "Мы помогаем найти для Вас новое жильё ВСЕГО за 25% комиссии.\n"
			+ "Так же у нас действуют уникальные акции и предложения для каждого нашего гостя!\n"
			+ "Для более подробной информации о наших услугах и контактах нажмите кнопку \"Полезная информация\" 🏘!";

	static final String emptyApartamentList = "Извините, нет данных по Вашему запросу. Пожалуйста, выберите другой раздел или другой район.";
	static final String wrongCommand = "Неправильная команда";
	static final String chooseApartament = "Пожалуйста, выберите подходящее Вам предложение.";

	static final String photoMainLogo = "main_logo";
	static final String photoMissing = "missing_foto";

	static final String addClientName = "Пожалуйста, отправьте нам Ваше имя чтобы мы знали как к Вам обращаться.";
	static final String addClientPhoneNumber = "Пожалуйста, отправьте нам Ваш номер телефона, чтобы мы знали как с Вами связаться.";

	static final String errorPhoneNumber = "Мы не знаем как с Вами связаться. Пожалуйста, отправьте нам Ваш номер телефона.";

	static final String photosSplitMarker = "!#!";
	static final String adminChatId = "308710480"; // 266367583 308710480 518987432
	static final String adminChatId2 = "518987432";
	static final String helloAdmin = "Приветствую, администратор ! Выберите дальшейшее действие.";

	static final String botEmainLogin = "arenda78spb@gmail.com";
	static final String botEmainPassword = "bxpscrew";
	static final String botDestinationEmail = "space_d@mail.ru"; // arenda78spb@gmail.com

	static final String newOrderFromTelegram = " Новая заявка из Telegram !";

	static final String RentRules = "Правила размещения объявления о сдаче квартиры в аренду на сервисе.\nНеобходимо заполнить все поля.\nПожалуйста, ознакомьтесь с правилами и нажмите кнопку \"Принимаю ➡️\".";

	static final String enterMetroStation = "Отправьте нам станцию ближайшего метро.\n(Например - Удельная)";
	static final String enterAddress = "Отправьте нам адрес жилья.\n(Например - ул. Правды 17)";
	static final String enterDescription = "Отправьте нам полное описание жилья. Не забудьте указать этаж, метраж и подобную важную информацию. ";
	static final String enterPrice = "Отправьте нам цену аренды жилья.\n(Например - 22000 + КУ)";
	static final String enterPhoto = "Отправьте нам фотографии жилья и нажмите \"Продолжить ➡️\"\nНа фото не должны присутствовать логотипы иных сервисов.";
	static final String preCheckData = "Проверьте данные и нажмите \"✅ Разместить объявление ✅\"";
	static final String postNewAdvertisement = "Ваша заявка отправлена на рассмотрение. В ближайшее время объявление станет доступно !";

	static final String exampleDescription = "1 этаж, кухня 8м, общая площадь 30м, есть вся необходимая мебель и техника, сдаётся для граждан РФ, можно с детьми, без больших животных.";

	static final String wrongAdminChatId = "Извините, вы не являетесь администратором.";
	static final String emptyRequests = "Извините, нет активных заявок на публикацию.";
	static final String addNewPost = "Новая заявка опубликована.";
	static final String deleteNewPost = "Объявление успешно удалено.";

	static final String happenException = "Извините, произошла внутренняя ошибка. Пожалуйста, повторите попытку.";
	static final String enterMassMsg = "Пожалуйста, отправьте текст для массовой рассылки.";
	static final String saveMassMsg = "Текст для массовой рассылки сохранен. Отправьте команду основному боту /sendMassToClients для отправки всем клиентам или /sendMassToRenters для отправки всем арендодателям.";
	static final String massMsgDelivery = "Массовое сообщение успешно отправлено.";

	static final String serviceInfo = "Вас Приветствует бот, в котором подобраны эксклюзивные варианты квартир и комнат! \n"
			+ "Мы поможем как снять, так и сдать любой объект недвижимости. Все наши предложения действующие и обновляются регулярно.\n"
			+ "\n" + "\n"
			+ "Мы уверены, что Вас привлечёт наш сервис, уровень обслуживания и конечно размер конкурентноспособной комиссии!\n"
			+ "Комиссия фиксирована и составит всего 25% !\n" + "\n" + "Взамен Вы получаете:\n" + "\n"
			+ "- Безопасность сделки! Так как мы не являемся информационным бюро, а оплата комиссии производится только после Вашего заселения.\n"
			+ "\n" + "- Эксклюзивное предложение по объекту, которого нет ни в одном другом агенстве \n" + "\n"
			+ "- Официальный договор через АН \"Адрес Плюс\"\n" + "\n"
			+ "- Юридическая поддержка на всех этапах работы с нами, то есть даже во время Вашего проживания в новой квартире!\n"
			+ "\n" + "По всем вопросам, а так же если Вы не нашли подходящего Вам варианта, можно обратиться:\n"
			+ "arenda78spb@gmail.com\n" + "А так же по телефонам :\n" + "+7(921)412-10-63 Иннокентий\n"
			+ "+7(931)581-21-22 Даниил";

}
