package ru.dgzorin.telegrambot;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import ru.dgzorin.database.Database;
import ru.dgzorin.log.Logger;

public class AdminBot extends TelegramLongPollingBot {

	private GeneralController generalController = new GeneralController();
	private SendMessage sendMessage = new SendMessage();
	private Keyboards chooseKeyboard = new Keyboards();
	private ArrayList<KeyboardRow> keyboard = new ArrayList<>();
	private HashMap<String, String> idList = new HashMap<>();
	private GetRentController getRentController = new GetRentController();
	private HashMap<String, String[]> getRentData = new HashMap<>();
	private HashMap<String, String[]> editData = new HashMap<>();

	private static HashMap<String, Boolean> renterMetroStationFlags = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterAddressFlags = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterDescriptionFlags = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterPrices = new HashMap<String, Boolean>();

	private boolean getRentFlag = false;
	private boolean getApartamentDataFlag = false; // флаг получения списка квартир по выбранным параметрам
	private boolean editFlag = false;
	private boolean massMsgFlag = false;

	private Logger logger = new Logger();

	private AdminBot(DefaultBotOptions options) {
		super(options);
	}

	private AdminBot() {
		super();
	}

	public static void startAdminBot(String ip, int port) throws Exception {

		HttpHost proxy = new HttpHost(ip, port);
		RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
		DefaultBotOptions options = new DefaultBotOptions();
		options.setRequestConfig(config);

		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			telegramBotsApi.registerBot(new AdminBot(options));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static void startAdminBot() throws Exception {

		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			telegramBotsApi.registerBot(new AdminBot());
		} catch (TelegramApiException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	@Override
	public String getBotUsername() {
		// return "arendavspb_admin_bot"; // rentbot
		return "spacetest2_bot"; // testbot2

	}

	@Override
	public String getBotToken() {
		// return "528935585:AAGYYf8RKmaBd6NXBcCHEQ6Y-5OKQBE9Uww";// rentbot
		return "566674566:AAFBoZPuBpWmk469FsX1UldFao64zMaF7Kw";// testbot2

	}

	@Override
	public void onUpdateReceived(Update update) {

		Message message = update.getMessage();

		Boolean renterMetroStationFlag = false;
		Boolean renterAddressFlag = false;
		Boolean renterDescriptionFlag = false;
		Boolean renterPriceFlag = false;

		if (message != null && message.hasText()) {
			String chatId = message.getChatId().toString();

			if (!message.getChatId().toString().equals(Constants.adminChatId)
					&& !message.getChatId().toString().equals(Constants.adminChatId2)) { // Constants.adminChatId
				try {
					execute(generalController.sendMsg(sendMessage, message, Constants.wrongAdminChatId));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				return;
			}

			if (renterMetroStationFlags.containsKey(chatId)) {
				renterMetroStationFlag = renterMetroStationFlags.get(chatId);
			}

			if (renterAddressFlags.containsKey(chatId)) {
				renterAddressFlag = renterAddressFlags.get(chatId);
			}

			if (renterDescriptionFlags.containsKey(chatId)) {
				renterDescriptionFlag = renterDescriptionFlags.get(chatId);
			}

			if (renterPrices.containsKey(chatId)) {
				renterPriceFlag = renterPrices.get(chatId);
			}

			switch (message.getText()) {

			case "Массовая рассылка": {
				try {
					execute(generalController.sendMsg(sendMessage, message, Constants.enterMassMsg));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				massMsgFlag = true;
				break;
			}

			case "/start":
			case "/admin": {

				keyboard = chooseKeyboard.adminStartMenuKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.helloAdmin));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;
			}
			case "🏡 Вернуться в главное меню 🏡": {
				getRentFlag = false;
				getApartamentDataFlag = false;
				editFlag = false;
				getRentData.remove(chatId);
				renterMetroStationFlags.remove(chatId);
				renterAddressFlags.remove(chatId);
				renterDescriptionFlags.remove(chatId);
				renterPrices.remove(chatId);

				keyboard = chooseKeyboard.adminStartMenuKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.helloAdmin));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;
			}

			case "Заявки на публикацию": {
				massMsgFlag = false;
				sendRequests(message);
				break;
			}

			case "⬅️ Выбрать другой район":
				keyboard = chooseKeyboard.choiseDistrictMenuKeyboard();
				sendDistrictList(message);
				break;

			case "✅ Опубликовать ✅": {
				newPost(idList.get(chatId));

				keyboard = chooseKeyboard.adminStartMenuKeyboard();
				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.addNewPost));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}

				idList.remove(chatId);
				break;
			}

			case "❌ Удалить": {
				deletePost(message, "requests", idList.get(chatId));
				break;
			}

			case "📝 Редактировать": {
				editData.put(chatId, Database.getEditData(idList.get(chatId)));
				editFlag = true;
				renterMetroStationFlags.put(chatId, true);

				keyboard = chooseKeyboard.adminEditMenuKeyboard();
				sendOldData(message, keyboard, 2);

				break;
			}

			case "Продолжить ➡️": {
				if (editFlag) {

					if (renterMetroStationFlag) {
						renterMetroStationFlags.remove(chatId);
						renterAddressFlags.put(chatId, true);
						sendOldData(message, keyboard, 3);
					}
					if (renterAddressFlag) {
						renterAddressFlags.remove(chatId);
						renterDescriptionFlags.put(chatId, true);
						sendOldData(message, keyboard, 5);
					}
					if (renterDescriptionFlag) {
						renterDescriptionFlags.remove(chatId);
						renterPrices.put(chatId, true);
						keyboard = chooseKeyboard.adminEditEndMenuKeyboard();
						sendOldData(message, keyboard, 6);
					}
				}

				break;
			}

			case "Сохранить изменения": {

				Database.setEditData(editData.get(chatId));
				keyboard = chooseKeyboard.adminStartMenuKeyboard();
				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, "Данные успешно сохранены."));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				editData.remove(chatId);
				editFlag = false;

				break;
			}

			/*
			 * ============================================================================
			 * 
			 * ============================================================================
			 */

			case "Удаление из рабочей области": {
				massMsgFlag = false;
				getRentFlag = true;
				getApartamentDataFlag = false;

				getRentData.put(message.getChatId().toString(), new String[3]);

				keyboard = chooseKeyboard.choiseDistrictMenuKeyboard();
				sendDistrictList(message);
				break;
			}

			case "🏢 Квартира": {
				keyboard = chooseKeyboard.rentGetApartamentKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.chooseSection));
				} catch (TelegramApiException e) {
					e.printStackTrace();
				}
				break;
			}

			case "🏢 Комната": {
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "roomOnly";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				break;
			}
			case "🏢 Студия": {
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "studio";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				break;
			}

			case "🏢 1-комнатная": {
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room1";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				break;
			}

			case "🏢 2-комнатная": {
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room2";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				break;
			}

			case "🏢 3-комнатная": {
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room3";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				break;
			}

			case "🏢 4-комнатная": {
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room4";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				break;
			}

			case "⬅️ Вернуться к списку": {
				sendApartamentList(message);
				break;
			}

			case "❌ Удалить объявление": {

				deletePost(message, getRentData.get(chatId)[1], getRentData.get(chatId)[2]);

				break;
			}

			default: {
				if (!editFlag && !massMsgFlag) {
					try {
						execute(generalController.sendMsg(sendMessage, message, Constants.wrongCommand));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}
			}
			}
		} else if (update.hasCallbackQuery()) {
			String call_data = update.getCallbackQuery().getData();
			String chatId2 = update.getCallbackQuery().getMessage().getChatId().toString();

			if (getApartamentDataFlag) { // получение списка фото по выбранной квартире и ее данных
				getRentData.get(update.getCallbackQuery().getMessage().getChatId().toString())[2] = call_data;

				ArrayList<SendPhoto> photoNamesList = getRentController.sendSelectedApartamentPhotos(chatId2,
						getRentData.get(chatId2)[1], getRentData.get(chatId2)[2]);

				for (SendPhoto sendPhotoRequest : photoNamesList) {
					try {
						sendPhoto(sendPhotoRequest);
					} catch (TelegramApiException ex) {
						ex.printStackTrace();
						sendExeptionToLog(message, ex);
					}
				}

				keyboard = chooseKeyboard.adminDeleteApartamentMenuKeyboard();

				try {
					execute(getRentController.sendSelectedApartamentData(chatId2, keyboard, getRentData.get(chatId2)[1],
							getRentData.get(chatId2)[2]));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}

				return;
			}

			if (getRentFlag) { // выбор района
				getRentData.get(chatId2)[0] = call_data;
				keyboard = chooseKeyboard.adminDelStep1MenuKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, update.getCallbackQuery().getMessage(), keyboard,
							Constants.chooseSection));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				return;
			}

			ArrayList<SendPhoto> photoNamesList = getRentController.sendSelectedApartamentPhotos(chatId2, "requests",
					call_data);

			for (SendPhoto sendPhotoRequest : photoNamesList) {
				try {
					sendPhoto(sendPhotoRequest);
				} catch (TelegramApiException ex) {
					ex.printStackTrace();
					sendExeptionToLog(message, ex);
				}
			}

			keyboard = chooseKeyboard.adminCRUDMenuKeyboard();

			try {
				execute(getRentController.sendSelectedApartamentData(chatId2, keyboard, "requests", call_data));
			} catch (TelegramApiException e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
			getRenterData(chatId2, call_data);
			idList.put(chatId2, call_data);

			return;
		}

		if (update.hasMessage() && editFlag) {
			if (renterMetroStationFlag) {
				stepEditData(message, keyboard, update.getMessage().getText(), 2);
			}
			if (renterAddressFlag) {
				stepEditData(message, keyboard, update.getMessage().getText(), 3);

			}
			if (renterDescriptionFlag) {
				stepEditData(message, keyboard, update.getMessage().getText(), 5);

			}
			if (renterPriceFlag) {
				stepEditData(message, keyboard, update.getMessage().getText(), 6);
			}
		}
		if (update.hasMessage() && massMsgFlag) {
			if (!update.getMessage().getText().equals("Заявки на публикацию")
					&& !update.getMessage().getText().equals("Удаление из рабочей области")
					&& !update.getMessage().getText().equals("Массовая рассылка")) {
				TelegramBot.setMassMsg(update.getMessage().getText());
				massMsgFlag = false;
				try {
					execute(generalController.sendMsg(sendMessage, message, Constants.saveMassMsg));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
			} else {
				return;
			}

		}

	}

	private void sendRequests(Message message) {

		if (!Database.checkRequests()) {
			try {
				execute(generalController.sendMsg(sendMessage, message, Constants.emptyRequests));
			} catch (TelegramApiException e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
			return;
		}

		for (SendPhoto sendPhotoRequest : sendRequestsList(message)) {
			try {
				sendPhoto(sendPhotoRequest);
			} catch (Exception e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
		}

	}

	private ArrayList<SendPhoto> sendRequestsList(Message message) {
		ArrayList<SendPhoto> apartamentList = new ArrayList<>();
		try {

			ResultSet rs = Database.getRequestsList();

			while (rs.next()) {

				String caption = "";
				String[] photoName = rs.getString(2).split(Constants.photosSplitMarker);

				String type = "";

				switch (rs.getString(8)) {
				case "room1":
					type = "1-комнатная";
					break;
				case "room2":
					type = "2-комнатная";

					break;
				case "room3":
					type = "3-комнатная";

					break;
				case "room4":
					type = "4-комнатная";

					break;
				case "roomOnly":
					type = "Комната";

					break;
				case "studio":
					type = "Студия";
					break;
				default:
					break;
				}

				caption += "Метро " + rs.getString(3) + "\n" + rs.getString(4) + "\n" + rs.getString(5) + "\nРайон : "
						+ rs.getString(7) + "\n" + type;

				SendPhoto sendPhotoRequest = new SendPhoto();
				File file;

				InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
				List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
				List<InlineKeyboardButton> rowInline = new ArrayList<>();
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					file = new File("photos/missing_foto.jpg");
				} else {
					file = new File("photos/get_rent/" + rs.getString(6) + "/" + photoName[0] + ".jpg");
				}

				rowInline.add(
						new InlineKeyboardButton().setText("Подробнее").setCallbackData(String.valueOf(rs.getInt(1))));

				rowsInline.add(rowInline);
				markupInline.setKeyboard(rowsInline);

				sendPhotoRequest.setChatId(message.getChatId().toString());

				sendPhotoRequest.setNewPhoto(file);
				sendPhotoRequest.setCaption(caption);
				sendPhotoRequest.setReplyMarkup(markupInline);

				apartamentList.add(sendPhotoRequest);
			}
			rs.close();
			return apartamentList;
		} catch (Exception e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
			return null;
		}
	}

	private void getRenterData(String chatId, String id) {

		ResultSet rs = Database.getRenterData(id);
		String name = "";
		String phone = "";
		try {
			name = rs.getString(1);
			phone = rs.getString(2);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.sendExeptionToLog(e);
		}

		String caption = "Рентор :\n" + name + "\n" + phone;

		SendMessage sendMessage = new SendMessage();

		sendMessage.setChatId(chatId);
		sendMessage.setText(caption);

		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			logger.sendExeptionToLog(e);
		}
	}

	private void newPost(String id) {

		Database.addNewPost(id);
		Database.removeRequest(id);

	}

	private void deletePost(Message message, String tableName, String id) {
		String chatId = message.getChatId().toString();
		try {
			ResultSet rs = Database.getSelectedApartamentPhotos(tableName, id);
			String[] photoNames = rs.getString(1).split(Constants.photosSplitMarker);

			for (String tempName : photoNames) {
				if (!(tempName == null) && !tempName.equals("")) {
					File file;
					file = new File("photos/get_rent/" + rs.getString(2) + "/" + tempName + ".jpg");
					file.delete();
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}

		Database.removePost(tableName, id);
		keyboard = chooseKeyboard.adminStartMenuKeyboard();

		try {
			execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.deleteNewPost));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
		idList.remove(chatId);

	}

	private void sendDistrictList(Message message) {
		try {
			sendPhoto(generalController.sendPhoto(message, keyboard, "districts/all", Constants.chooseDistrict));
		} catch (TelegramApiException ex) {
			ex.printStackTrace();
		}

		for (SendPhoto sendPhotoRequest : generalController.sendDistrictList(sendMessage, message)) {
			try {
				sendPhoto(sendPhotoRequest);
			} catch (TelegramApiException ex) {
				ex.printStackTrace();
				sendExeptionToLog(message, ex);
			}
		}

		try {
			execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.chooseDistrict));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void sendApartamentList(Message message) {

		if (!getRentController.checkApartamentList(message, getRentData.get(message.getChatId().toString())[1],
				getRentData.get(message.getChatId().toString())[0])) {
			try {
				execute(generalController.sendMsg(sendMessage, message, Constants.emptyApartamentList));
			} catch (TelegramApiException e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
			return;
		}

		for (SendPhoto sendPhotoRequest : getRentController.sendApartamentList(message,
				getRentData.get(message.getChatId().toString())[1],
				getRentData.get(message.getChatId().toString())[0])) {
			try {
				sendPhoto(sendPhotoRequest);
			} catch (Exception e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
		}

		try {
			keyboard = chooseKeyboard.rentGetApartamentKeyboard();
			execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.chooseApartament));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void sendOldData(Message message, ArrayList<KeyboardRow> keyboard, int index) {
		try {
			execute(generalController.sendMsgNoReply(message, keyboard,
					"Старые данные:\n" + editData.get(message.getChatId().toString())[index]));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}

		try {
			execute(generalController.sendMsgNoReply(message, keyboard,
					"Нажмите Продолжить или отправьте новые данные"));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void stepEditData(Message message, ArrayList<KeyboardRow> keyboard, String data, int dataId) {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);
		String chatId = message.getChatId().toString();

		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"Продолжить ➡️\" если данные верны, либо отправьте заново.";
		if (!data.equals("Продолжить ➡️") && !data.equals("🏡 Вернуться в главное меню 🏡")
				&& !data.equals("Сохранить изменения")) {

			editData.get(chatId)[dataId] = data;

		} else {
			return;
		}
		sendMessage.setText(nextStep);

		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void sendExeptionToLog(Message message, Exception e) {
		logger.sendExeptionToLog(e);
		keyboard = chooseKeyboard.adminStartMenuKeyboard();
		try {
			execute(generalController.sendMsgNoReply(message, keyboard, Constants.happenException));
		} catch (TelegramApiException exc) {
			logger.sendExeptionToLog(exc);
		}
	}

}
