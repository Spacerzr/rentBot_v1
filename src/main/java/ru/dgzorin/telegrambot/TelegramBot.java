package ru.dgzorin.telegrambot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.GetFile;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.PhotoSize;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import ru.dgzorin.database.Database;
import ru.dgzorin.log.Logger;

public class TelegramBot extends TelegramLongPollingBot {

	private GeneralController generalController = new GeneralController();
	private GetRentController getRentController = new GetRentController();
	private SetRentController setRentController = new SetRentController();
	private Logger logger = new Logger();

	private MailSender emailSender = new MailSender(Constants.botEmainLogin, Constants.botEmainPassword);
	private SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy 'at' HH.mm ':'");
	private SendMessage sendMessage = new SendMessage();
	private Keyboards chooseKeyboard = new Keyboards();
	private ArrayList<KeyboardRow> keyboard = new ArrayList<>();
	private HashMap<String, String[]> getRentData = new HashMap<>(); // ключ chatId, [0]-район [1]-название таблицы
																		// [2] - id квартиры
	private HashMap<String, String[]> setRentData = new HashMap<>();

	private static HashMap<String, String> clientNames = new HashMap<String, String>();
	private static HashMap<String, Boolean> clientNamesFlags = new HashMap<String, Boolean>();

	private static HashMap<String, String> phoneNumbers = new HashMap<String, String>();
	private static HashMap<String, Boolean> PhoneNumbersFlags = new HashMap<String, Boolean>();

	private static HashMap<String, String> renterNames = new HashMap<String, String>();
	private static HashMap<String, Boolean> renterNamesFlags = new HashMap<String, Boolean>();

	private static HashMap<String, String> renterPhoneNumbers = new HashMap<String, String>();
	private static HashMap<String, Boolean> renterPhoneNumbersFlags = new HashMap<String, Boolean>();

	private static HashMap<String, Boolean> renterMetroStationFlags = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterAddressFlags = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterDescriptionFlags = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterPrices = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterPhotoFlags = new HashMap<String, Boolean>();
	private static HashMap<String, Boolean> renterStartRegFlags = new HashMap<String, Boolean>();

	private static HashMap<String, String> preCheckAdvertisements = new HashMap<String, String>();

	private boolean getRentFlag = false; // флаг раздела "Снять"
	private boolean setRentFlag = false; // флаг раздела "Сдать"
	private boolean getApartamentDataFlag = false; // флаг получения списка квартир по выбранным параметрам

	private static String massMsg = "";

	private TelegramBot(DefaultBotOptions options) {
		super(options);
	}

	private TelegramBot() {
		super();
	}

	public static void setMassMsg(String massMsg) {
		TelegramBot.massMsg = massMsg;
	}

	public static void startMainBot(String ip, int port) {

		HttpHost proxy = new HttpHost(ip, port);
		RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
		DefaultBotOptions options = new DefaultBotOptions();
		options.setRequestConfig(config);

		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			telegramBotsApi.registerBot(new TelegramBot(options));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static void startMainBot() throws Exception {

		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			telegramBotsApi.registerBot(new TelegramBot());
		} catch (TelegramApiException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	@Override
	public String getBotUsername() {
		// return "arendavspb_bot"; // rentbot
		return "spacetest_bot"; // rentbot

	}

	@Override
	public String getBotToken() {
		// return "499445368:AAGdaJSGbVpz-EJxcr3RuOWGzB1L4IrCilU"; // rentbot
		return "540215721:AAEWeyYcquJJu11DgjC---EPflI2LSbR7VM"; // testbot

	}

	@Override
	public void onUpdateReceived(Update update) {

		Message message = update.getMessage();

		Boolean clientNameFlag = false;
		Boolean phoneNumberFlag = false;

		// флаги для входных данных раздела "Снять"
		Boolean renterNameFlag = false;
		Boolean renterPhoneNumberFlag = false;
		Boolean renterMetroStationFlag = false;
		Boolean renterAddressFlag = false;
		Boolean renterDescriptionFlag = false;
		Boolean renterPriceFlag = false;
		Boolean renterPhotoFlag = false;
		Boolean renterStartRegFlag = false;

		// Boolean adminFlag = false;

		if (message != null && message.hasText()) {
			String chatId = message.getChatId().toString();
			// String clientName = message.getFrom().getFirstName() + " " +
			// message.getFrom().getLastName() + "(@"
			// + message.getFrom().getUserName() + ")";
			// System.out.println(clientName);

			if (PhoneNumbersFlags.containsKey(chatId)) {
				phoneNumberFlag = PhoneNumbersFlags.get(chatId);
			}
			if (clientNamesFlags.containsKey(chatId)) {
				clientNameFlag = clientNamesFlags.get(chatId);
			}

			// флаги раздела "Снять"

			if (renterMetroStationFlags.containsKey(chatId)) {
				renterMetroStationFlag = renterMetroStationFlags.get(chatId);
			}

			if (renterAddressFlags.containsKey(chatId)) {
				renterAddressFlag = renterAddressFlags.get(chatId);
			}

			if (renterDescriptionFlags.containsKey(chatId)) {
				renterDescriptionFlag = renterDescriptionFlags.get(chatId);
			}

			if (renterPrices.containsKey(chatId)) {
				renterPriceFlag = renterPrices.get(chatId);
			}

			if (renterPhotoFlags.containsKey(chatId)) {
				renterPhotoFlag = renterPhotoFlags.get(chatId);
			}

			if (renterPhoneNumbersFlags.containsKey(chatId)) {
				renterPhoneNumberFlag = renterPhoneNumbersFlags.get(chatId);
			}
			if (renterNamesFlags.containsKey(chatId)) {
				renterNameFlag = renterNamesFlags.get(chatId);
			}

			if (renterStartRegFlags.containsKey(chatId)) {
				renterStartRegFlag = renterStartRegFlags.get(chatId);
			}

			/*
			 * ============================================================================
			 * обработка кнопок
			 * ============================================================================
			 */

			switch (message.getText()) {

			/*
			 * ============================================================================
			 * обработка основных кнопок
			 * ============================================================================
			 */

			case "/start":
			case "🏡 Вернуться в главное меню 🏡":

				getRentData.remove(message.getChatId().toString());
				getRentFlag = false;
				setRentFlag = false;
				getApartamentDataFlag = false;
				renterMetroStationFlags.remove(chatId);
				renterAddressFlags.remove(chatId);
				renterDescriptionFlags.remove(chatId);
				renterPrices.remove(chatId);
				renterPhotoFlags.remove(chatId);
				renterNamesFlags.remove(chatId);
				renterPhoneNumbersFlags.remove(chatId);

				sendStartMessage(message);
				break;

			case "☑️ Полезная информация ☑️":
				keyboard = chooseKeyboard.startMenuKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.serviceInfo));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;

			case "🏘 Сдать":
				setRentFlag = true;
				getRentFlag = false;
				getApartamentDataFlag = false;
				keyboard = chooseKeyboard.rentSetStepRulesKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.RentRules));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;
			case "🏘 Снять":
				searchClientInDb(message.getChatId().toString());
				getApartamentDataFlag = false;
				getRentFlag = true;
				setRentFlag = false;
				getRentData.put(message.getChatId().toString(), new String[3]);

				keyboard = chooseKeyboard.choiseDistrictMenuKeyboard();
				sendDistrictList(message);
				break;

			case "⬅️ Выбрать другой район":
				getApartamentDataFlag = false;
				keyboard = chooseKeyboard.choiseDistrictMenuKeyboard();
				sendDistrictList(message);

				break;
			case "🏢 Студия":
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "studio";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				if (setRentFlag) {
					setRentData.get(update.getMessage().getChatId().toString())[1] = "studio";
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterMetroStation));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					renterMetroStationFlags.put(chatId, true);
					break;
				}
				break;

			case "🏢 1-комнатная":
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room1";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}

				if (setRentFlag) {
					setRentData.get(update.getMessage().getChatId().toString())[1] = "room1";
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterMetroStation));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					renterMetroStationFlags.put(chatId, true);

					break;
				}
				break;

			case "🏢 2-комнатная":
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room2";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}

				if (setRentFlag) {
					setRentData.get(update.getMessage().getChatId().toString())[1] = "room2";
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterMetroStation));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					renterMetroStationFlags.put(chatId, true);
					break;
				}
				break;

			case "🏢 3-комнатная":
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room3";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}

				if (setRentFlag) {
					setRentData.get(update.getMessage().getChatId().toString())[1] = "room3";
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterMetroStation));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					renterMetroStationFlags.put(chatId, true);
					break;
				}
				break;

			case "🏢 4-комнатная":
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "room4";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}

				if (setRentFlag) {
					setRentData.get(update.getMessage().getChatId().toString())[1] = "room4";
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterMetroStation));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					renterMetroStationFlags.put(chatId, true);
					break;
				}
				break;

			/*
			 * ============================================================================
			 * обработка кнопок раздела "Снять"
			 * ============================================================================
			 */

			case "🏢 Снять квартиру":
				getApartamentDataFlag = false;
				keyboard = chooseKeyboard.rentGetApartamentKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.chooseSection));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}

				break;
			case "🏢 Снять комнату":
				if (getRentFlag) {
					getRentData.get(update.getMessage().getChatId().toString())[1] = "roomOnly";
					getApartamentDataFlag = true;
					sendApartamentList(message);
					break;
				}
				break;

			/*
			 * ============================================================================
			 * обработка кнопок раздела "Снять"
			 * ============================================================================
			 */

			case "Принимаю ➡️":
				searchRenterInDb(chatId);

				getApartamentDataFlag = false;
				getRentFlag = false;
				setRentFlag = true;

				setRentData.put(message.getChatId().toString(), new String[10]);
				setRentData.get(chatId)[6] = "";

				String msgToStep2 = "";

				String name2 = Database.getRenterName(chatId);
				String phoneNumber3 = Database.getRenterPhoneNumber(chatId);

				if (!(name2 == null) && !(phoneNumber3 == null) && !name2.equals(null) && !phoneNumber3.equals(null)
						&& !name2.equals("") && !phoneNumber3.equals("")) {
					msgToStep2 = "Вы уже вводили ранее данные: \n" + "Ваше имя - \"" + name2 + "\";\n" + "телефон - \""
							+ phoneNumber3 + "\".\n"
							+ "Если данные актуальны, нажмите \"Продолжить ➡️\",либо \"🔃 Изменить\" для их редактирования.";

					renterStartRegFlags.put(chatId, true);
					keyboard = chooseKeyboard.rentSetStepNameKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, msgToStep2));
					} catch (TelegramApiException e) {
						e.printStackTrace();
					}
					break;
				} else {
					msgToStep2 = Constants.addClientName;
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					renterNamesFlags.put(chatId, true);
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, msgToStep2));
					} catch (TelegramApiException e) {
						e.printStackTrace();
					}
					break;
				}

			case "🏢 Сдать квартиру":
				keyboard = chooseKeyboard.rentGetApartamentKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.chooseSection));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;

			case "🏢 Сдать комнату":
				if (setRentFlag) {
					setRentData.get(update.getMessage().getChatId().toString())[1] = "roomOnly";
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterMetroStation));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					renterMetroStationFlags.put(chatId, true);
					break;
				}
				break;

			/*
			 * ============================================================================
			 * Оформление заявки.
			 * ============================================================================
			 */

			case "✅ Снять эту квартиру":

				String msgToStep1 = "";

				String name = Database.getClientName(chatId);
				String phoneNumber = Database.getClientPhoneNumber(chatId);

				if (!(name == null) && !(phoneNumber == null) && !name.equals(null) && !phoneNumber.equals(null)
						&& !name.equals("") && !phoneNumber.equals("")) {
					msgToStep1 = "Вы уже вводили ранее данные: \n" + "Ваше имя - \"" + name + "\";\n" + "телефон - \""
							+ phoneNumber + "\".\n"
							+ "Если данные актуальны, нажмите \"✅ Отправить заявку ✅\",либо \"🔃 Изменить\" для их редактирования.";

					keyboard = chooseKeyboard.rentGetStepRegRequestKeyboard();
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, msgToStep1));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				} else {
					msgToStep1 = Constants.addClientName;
					keyboard = chooseKeyboard.rentGetStepNameKeyboard();
					clientNamesFlags.put(chatId, true);
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, msgToStep1));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}

			case "Продолжить ➡️":
				if (getRentFlag) {
					// кнопка перехода из имени в телефон
					String clientName = Database.getClientName(chatId);

					if ((clientName == null) || clientName.equals("") && !clientName.equals(null)) {
						String errorAddressMsg = "Мы не знаем как к Вам обращаться. Пожалуйста, отправьте нам Ваше имя.";
						keyboard = chooseKeyboard.rentGetStepNameKeyboard();

						try {
							execute(generalController.sendMsg(sendMessage, message, keyboard, errorAddressMsg));
						} catch (TelegramApiException e) {
							e.printStackTrace();
							sendExeptionToLog(message, e);
						}
						break;
					}
					clientNamesFlags.remove(chatId);
					PhoneNumbersFlags.put(chatId, true);

					keyboard = chooseKeyboard.rentGetStepPhoneNumberKeyboard();
					String msg2 = Constants.addClientPhoneNumber;
					String phoneNumber2 = Database.getClientPhoneNumber(chatId);

					if (!(phoneNumber2 == null) && !phoneNumber2.equals("") && !phoneNumber2.equals(null)) {
						msg2 = "Вы уже вводили ранее номер телефона - \"" + phoneNumber2
								+ "\". Если он актуален, нажмите \"Отправить заявку\", либо отправьте нам новый номер";
						phoneNumbers.put(chatId, phoneNumber2);
					}
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, msg2));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}

					break;
				}
				if (setRentFlag) {

					if (renterStartRegFlag) {

						renterStartRegFlags.put(chatId, false);
						keyboard = chooseKeyboard.choiseDistrictMenuKeyboard();
						sendDistrictList(message);

						break;
					}

					if (renterNameFlag) {
						String renterName = Database.getRenterName(chatId);

						if ((renterName == null) || renterName.equals("") && !renterName.equals(null)) {
							String errorAddressMsg = "Мы не знаем как к Вам обращаться. Пожалуйста, отправьте нам Ваше имя.";
							keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();

							try {
								execute(generalController.sendMsg(sendMessage, message, keyboard, errorAddressMsg));
							} catch (TelegramApiException e) {
								e.printStackTrace();
								sendExeptionToLog(message, e);
							}
							break;
						}
						renterNamesFlags.remove(chatId);
						renterPhoneNumbersFlags.put(chatId, true);

						keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
						String msg2 = Constants.addClientPhoneNumber;
						String phoneNumber2 = Database.getRenterPhoneNumber(chatId);

						if (!(phoneNumber2 == null) && !phoneNumber2.equals("") && !phoneNumber2.equals(null)) {
							msg2 = "Вы уже вводили ранее номер телефона - \"" + phoneNumber2
									+ "\". Если он актуален, нажмите \"Продолжить ➡️\", либо отправьте нам новый номер";
							renterPhoneNumbers.put(chatId, phoneNumber2);
						}
						try {
							execute(generalController.sendMsg(sendMessage, message, keyboard, msg2));
						} catch (TelegramApiException e) {
							e.printStackTrace();
							sendExeptionToLog(message, e);
						}

						break;
					}

					if (renterPhoneNumberFlag) {
						String phoneNumer = Database.getRenterPhoneNumber(chatId);
						if ((phoneNumer == null) || phoneNumer.equals("") && !phoneNumer.equals(null)) {

							keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();

							try {
								execute(generalController.sendMsg(sendMessage, message, keyboard,
										Constants.errorPhoneNumber));
							} catch (TelegramApiException e) {
								e.printStackTrace();
								sendExeptionToLog(message, e);
							}
							break;
						}

						renterPhoneNumbersFlags.remove(chatId);

						renterStartRegFlags.put(chatId, false);
						keyboard = chooseKeyboard.choiseDistrictMenuKeyboard();
						sendDistrictList(message);

						break;

					}

					if (renterMetroStationFlag) {

						keyboard = chooseKeyboard.rentApartamentStepKeyboard();

						renterMetroStationFlags.put(chatId, false);
						renterAddressFlags.put(chatId, true);

						try {
							execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterAddress));
						} catch (TelegramApiException e) {
							e.printStackTrace();
							sendExeptionToLog(message, e);
						}
						break;
					}

					if (renterAddressFlag) {

						renterAddressFlags.put(chatId, false);
						renterDescriptionFlags.put(chatId, true);
						keyboard = chooseKeyboard.rentApartamentStepWithExampleKeyboard();

						try {
							execute(generalController.sendMsg(sendMessage, message, keyboard,
									Constants.enterDescription));
						} catch (TelegramApiException e) {
							e.printStackTrace();
							sendExeptionToLog(message, e);
						}
						break;
					}

					if (renterDescriptionFlag) {

						renterDescriptionFlags.put(chatId, false);
						renterPrices.put(chatId, true);
						keyboard = chooseKeyboard.rentApartamentStepKeyboard();

						try {
							execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterPrice));
						} catch (TelegramApiException e) {
							e.printStackTrace();
							sendExeptionToLog(message, e);
						}
						break;
					}

					if (renterPriceFlag) {

						renterPrices.put(chatId, false);
						renterPhotoFlags.put(chatId, true);

						try {
							execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterPhoto));
						} catch (TelegramApiException e) {
							e.printStackTrace();
							sendExeptionToLog(message, e);
						}

						break;
					}

					if (renterPhotoFlag) {
						renterPhotoFlags.put(chatId, false);
						keyboard = chooseKeyboard.rentApartamentPreCheckDataKeyboard();
						preCheckOrder(message);
						try {
							execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.preCheckData));
						} catch (TelegramApiException e) {
							e.printStackTrace();
							sendExeptionToLog(message, e);
						}
					}
				}
				break;
			case "⬅️ Назад":
				if (renterPhotoFlag) {
					renterPhotoFlags.put(chatId, false);
					renterPrices.put(chatId, true);

					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterPrice));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
				}
				if (renterPriceFlag) {
					renterPrices.put(chatId, false);
					renterDescriptionFlags.put(chatId, true);
					keyboard = chooseKeyboard.rentApartamentStepWithExampleKeyboard();

					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterDescription));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
				}

				if (renterDescriptionFlag) {
					renterDescriptionFlags.put(chatId, false);
					renterAddressFlags.put(chatId, true);
					keyboard = chooseKeyboard.rentApartamentStepKeyboard();

					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterAddress));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
				}
				if (renterAddressFlag) {
					renterAddressFlags.put(chatId, false);
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();

					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.enterMetroStation));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}

				}
				break;

			case "🔃 Изменить":
				if (getRentFlag) {
					clientNamesFlags.put(chatId, true);
					keyboard = chooseKeyboard.rentGetStepNameKeyboard();
					String msg3 = Constants.addClientName;
					String name1 = Database.getClientName(chatId);
					if (!(name1 == null) && !name1.equals("") && !name1.equals(null)) {
						msg3 = "Вы уже вводили ранее Ваше имя - \"" + name1
								+ "\". Если оно актуально, нажмите \"Продолжить ➡️\", либо отправьте нам новое имя.";
						clientNames.put(chatId, name1);

					}
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, msg3));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}
				if (setRentFlag) {
					renterStartRegFlags.put(chatId, false);

					renterNamesFlags.put(chatId, true);
					keyboard = chooseKeyboard.rentApartamentStepNoBackKeyboard();
					String msg3 = Constants.addClientName;
					String name1 = Database.getRenterName(chatId);
					if (!(name1 == null) && !name1.equals("") && !name1.equals(null)) {
						msg3 = "Вы уже вводили ранее Ваше имя - \"" + name1
								+ "\". Если оно актуально, нажмите \"Продолжить ➡️\", либо отправьте нам новое имя.";
						renterNames.put(chatId, name1);

					}
					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, msg3));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}

			case "🔎 Посмотреть пример описания 🔎":
				try {
					execute(generalController.sendMsg(sendMessage, message, Constants.exampleDescription));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;

			case "✅ Отправить заявку ✅":
				String phoneNumer = Database.getClientPhoneNumber(chatId);
				if ((phoneNumer == null) || phoneNumer.equals("") && !phoneNumer.equals(null)) {
					keyboard = chooseKeyboard.rentGetStepPhoneNumberKeyboard();

					try {
						execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.errorPhoneNumber));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}

				PhoneNumbersFlags.remove(chatId);

				String clientName1 = Database.getClientName(chatId);

				String order = getRentController.getFinishOrder(chatId, clientName1, phoneNumer,
						getRentData.get(chatId)[1], getRentData.get(chatId)[2]);

				try {
					execute(generalController.sendRequestToAdmin(order));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}

				emailSender.send("" + date.format(new Date()) + Constants.newOrderFromTelegram, order,
						Constants.botDestinationEmail);

				keyboard = chooseKeyboard.startMenuKeyboard();
				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard,
							"Ваша заявка оформлена ! Наш менеджер свяжется с Вами в ближайшее время."));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				getRentData.remove(chatId);
				clientNamesFlags.remove(chatId);
				PhoneNumbersFlags.remove(chatId);

				break;

			case "⬅️ Вернуться к списку":
				clientNamesFlags.remove(chatId);
				PhoneNumbersFlags.remove(chatId);
				sendApartamentList(message);

				break;

			case "✅ Разместить объявление ✅":
				Database.postNewRequest(chatId, setRentData.get(chatId));
				String setOrder = setRentController.getFinishOrder(chatId, setRentData.get(chatId));
				try {
					execute(generalController.sendRequestToAdmin(setOrder));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}

				emailSender.send("" + date.format(new Date()) + Constants.newOrderFromTelegram, setOrder,
						Constants.botDestinationEmail);

				keyboard = chooseKeyboard.startMenuKeyboard();
				try {
					execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.postNewAdvertisement));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}

				setRentData.remove(chatId);
				setRentFlag = false;
				getRentFlag = false;
				break;

			case "/sendMassToRenters":
				if (!message.getChatId().toString().equals(Constants.adminChatId)
						&& !message.getChatId().toString().equals(Constants.adminChatId2)) { //
					try {
						execute(generalController.sendMsg(sendMessage, message, Constants.wrongCommand));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}
				try {
					for (SendMessage tmp : generalController.sendMassMsgToRenters(massMsg)) {
						execute(tmp);
					}
				} catch (SQLException | TelegramApiException e1) {
					e1.printStackTrace();
					sendExeptionToLog(message, e1);
				}

				try {
					execute(generalController.sendMsg(sendMessage, message, Constants.massMsgDelivery));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;

			case "/sendMassToClients":
				if (!message.getChatId().toString().equals("266367583")
						&& !message.getChatId().toString().equals("308710480")) { // Constants.adminChatId
					try {
						execute(generalController.sendMsg(sendMessage, message, Constants.wrongCommand));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}
				try {

					for (SendMessage tmp : generalController.sendMassMsgToClients(massMsg)) {
						try {
							execute(tmp);
						} catch (TelegramApiException e2) {
							e2.printStackTrace();
							logger.sendExeptionToLog(e2);
						}
					}

				} catch (SQLException e1) {
					e1.printStackTrace();
					sendExeptionToLog(message, e1);
				}

				try {
					execute(generalController.sendMsg(sendMessage, message, Constants.massMsgDelivery));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
				break;

			default:
				if (!clientNameFlag && !phoneNumberFlag && !renterMetroStationFlag && !renterAddressFlag
						&& !renterDescriptionFlag && !renterPriceFlag && !renterPhotoFlag && !renterNameFlag
						&& !renterPhoneNumberFlag) {
					try {
						execute(generalController.sendMsg(sendMessage, message, Constants.wrongCommand));
					} catch (TelegramApiException e) {
						e.printStackTrace();
						sendExeptionToLog(message, e);
					}
					break;
				}
			}
		}

		/*
		 * ============================================================================
		 * обработка inline кнопок
		 * ============================================================================
		 */

		else if (update.hasCallbackQuery() && !clientNameFlag && !phoneNumberFlag && !renterMetroStationFlag
				&& !renterAddressFlag && !renterDescriptionFlag && !renterPriceFlag && !renterPhotoFlag
				&& !renterNameFlag && !renterPhoneNumberFlag) {
			String call_data = update.getCallbackQuery().getData();
			String chatId2 = update.getCallbackQuery().getMessage().getChatId().toString();

			if (getApartamentDataFlag) { // получение списка фото по выбранной квартире и ее данных
				getRentData.get(update.getCallbackQuery().getMessage().getChatId().toString())[2] = call_data;

				ArrayList<SendPhoto> photoNamesList = getRentController.sendSelectedApartamentPhotos(chatId2,
						getRentData.get(chatId2)[1], getRentData.get(chatId2)[2]);

				for (SendPhoto sendPhotoRequest : photoNamesList) {
					try {
						sendPhoto(sendPhotoRequest);
					} catch (TelegramApiException ex) {
						ex.printStackTrace();
						sendExeptionToLog(message, ex);
					}
				}

				keyboard = chooseKeyboard.rentApartmentMenuKeyboard();

				try {
					execute(getRentController.sendSelectedApartamentData(chatId2, keyboard, getRentData.get(chatId2)[1],
							getRentData.get(chatId2)[2]));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}

				return;
			}
			if (getRentFlag) { // выбор внутри раздела "Снять" района
				getRentData.get(chatId2)[0] = call_data;
				keyboard = chooseKeyboard.rentGetMenuKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, update.getCallbackQuery().getMessage(), keyboard,
							Constants.chooseSection));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
			}
			if (setRentFlag) { // выбор внутри раздела "Сдать" района
				setRentData.get(chatId2)[0] = call_data;
				keyboard = chooseKeyboard.rentSetMenuKeyboard();

				try {
					execute(generalController.sendMsg(sendMessage, update.getCallbackQuery().getMessage(), keyboard,
							Constants.chooseSection));
				} catch (TelegramApiException e) {
					e.printStackTrace();
					sendExeptionToLog(message, e);
				}
			}
		}

		/*
		 * ============================================================================
		 * обработка входных данных раздела "Снять"
		 * ============================================================================
		 */

		if (update.hasMessage()) {
			// раздел "Снять"
			if (getRentFlag) {
				if (clientNameFlag) {
					regClientStepName(message, keyboard, update.getMessage().getText());
				}
				if (phoneNumberFlag) {
					regClientStepPhoneNumber(message, keyboard, update.getMessage().getText());
				}
			}
			if (setRentFlag) {
				// раздел "Сдать"
				if (renterNameFlag) {
					regRenterStepName(message, keyboard, update.getMessage().getText());
				}
				if (renterPhoneNumberFlag) {
					regRenterStepPhoneNumber(message, keyboard, update.getMessage().getText());
				}
				if (renterMetroStationFlag) {
					regApartamentStepData(message, keyboard, update.getMessage().getText(), 2);
				}
				if (renterAddressFlag) {
					regApartamentStepData(message, keyboard, update.getMessage().getText(), 3);
				}
				if (renterDescriptionFlag) {
					regApartamentStepData(message, keyboard, update.getMessage().getText(), 4);
				}
				if (renterPriceFlag) {
					regApartamentStepData(message, keyboard, update.getMessage().getText(), 5);
				}
				if (update.getMessage().hasPhoto()
						&& renterPhotoFlags.get(update.getMessage().getChatId().toString())) {
					regApartamentStepPhoto(message, 6);
				}
			}
		}
	}

	/*
	 * ============================================================================
	 * внутренние методы
	 * ============================================================================
	 */

	private void sendApartamentList(Message message) {

		if (!getRentController.checkApartamentList(message, getRentData.get(message.getChatId().toString())[1],
				getRentData.get(message.getChatId().toString())[0])) {
			try {
				execute(generalController.sendMsg(sendMessage, message, Constants.emptyApartamentList));
			} catch (TelegramApiException e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
			return;
		}

		for (SendPhoto sendPhotoRequest : getRentController.sendApartamentList(message,
				getRentData.get(message.getChatId().toString())[1],
				getRentData.get(message.getChatId().toString())[0])) {
			try {
				sendPhoto(sendPhotoRequest);
			} catch (Exception e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
		}

		try {
			keyboard = chooseKeyboard.rentGetApartamentKeyboard();
			execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.chooseApartament));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void sendStartMessage(Message message) {
		String chatId = message.getChatId().toString();
		getRentData.remove(chatId);
		clientNamesFlags.remove(chatId);
		PhoneNumbersFlags.remove(chatId);

		keyboard = chooseKeyboard.startMenuKeyboard();

		try {
			sendPhoto(generalController.sendPhoto(message, keyboard, Constants.photoMainLogo, Constants.startMessage));
		} catch (TelegramApiException ex) {
			try {
				sendPhoto(generalController.sendPhoto(message, keyboard, Constants.photoMissing,
						Constants.chooseDistrict));
			} catch (TelegramApiException e) {
				e.printStackTrace();
				sendExeptionToLog(message, e);
			}
		}

		try {
			execute(generalController.sendMsgNoReply(message, Constants.startMessage2));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void sendDistrictList(Message message) {
		try {
			sendPhoto(generalController.sendPhoto(message, keyboard, "districts/all", Constants.chooseDistrict));
		} catch (TelegramApiException ex) {
			ex.printStackTrace();
			sendExeptionToLog(message, ex);
		}

		for (SendPhoto sendPhotoRequest : generalController.sendDistrictList(sendMessage, message)) {
			try {
				sendPhoto(sendPhotoRequest);
			} catch (TelegramApiException ex) {
				ex.printStackTrace();
				sendExeptionToLog(message, ex);
			}
		}

		try {
			execute(generalController.sendMsg(sendMessage, message, keyboard, Constants.chooseDistrict));
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void searchClientInDb(String chatId) {

		if (Database.getClientChatId(chatId)) {
			return;
		}
		Database.addNewClient(chatId);
	}

	private void searchRenterInDb(String chatId) {

		if (Database.getRenterChatId(chatId)) {
			return;
		}
		Database.addNewRenter(chatId);
	}

	/*
	 * ============================================================================
	 * раздел "Снять"
	 * ============================================================================
	 */

	private void regClientStepName(Message message, ArrayList<KeyboardRow> keyboard, String name) {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);
		String chatId = message.getChatId().toString();
		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(message.getMessageId());
		String nextStep = "Нажмите кнопку \"Продолжить ➡️\" если имя верно, либо отправьте нам другое имя.";
		if (!name.equals("Продолжить ➡️") && !name.equals("🏡 Вернуться в главное меню 🏡")
				&& !name.equals("⬅️ Вернуться к списку")) {
			clientNames.put(chatId, name);
			Database.setClientName(chatId, name);
		} else {
			return;
		}
		sendMessage.setText(nextStep);

		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void regClientStepPhoneNumber(Message message, ArrayList<KeyboardRow> keyboard, String phoneNumber) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);
		String chatId = message.getChatId().toString();

		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"✅ Отправить заявку ✅\" если номер телефона верный, либо отправьте нам другой номер.";
		if (!phoneNumber.equals("✅ Отправить заявку ✅") && !phoneNumber.equals("🏡 Вернуться в главное меню 🏡")
				&& !phoneNumber.equals("⬅️ Вернуться к списку")) {
			phoneNumbers.put(chatId, phoneNumber);
			Database.setClientPhoneNumber(chatId, phoneNumber);
		} else {
			return;
		}

		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	/*
	 * ============================================================================
	 * раздел "Сдать"
	 * ============================================================================
	 */

	private void regRenterStepName(Message message, ArrayList<KeyboardRow> keyboard, String name) {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);
		String chatId = message.getChatId().toString();
		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"Продолжить ➡️\" если имя верно, либо отправьте нам другое имя.";
		if (!name.equals("Продолжить ➡️") && !name.equals("🏡 Вернуться в главное меню 🏡")) {
			renterNames.put(chatId, name);
			Database.setRenterName(chatId, name);
		} else {
			return;
		}
		sendMessage.setText(nextStep);

		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void regRenterStepPhoneNumber(Message message, ArrayList<KeyboardRow> keyboard, String phoneNumber) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);
		String chatId = message.getChatId().toString();

		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"Продолжить ➡️\" если номер телефона верный, либо отправьте нам другой номер.";
		if (!phoneNumber.equals("Продолжить ➡️") && !phoneNumber.equals("🏡 Вернуться в главное меню 🏡")) {
			renterPhoneNumbers.put(chatId, phoneNumber);
			Database.setRenterPhoneNumber(chatId, phoneNumber);
		} else {
			return;
		}

		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void regApartamentStepData(Message message, ArrayList<KeyboardRow> keyboard, String data, int dataId) {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);
		String chatId = message.getChatId().toString();

		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"Продолжить ➡️\" если данные верны, либо отправьте заново.";
		if (!data.equals("Продолжить ➡️") && !data.equals("🏡 Вернуться в главное меню 🏡") && !data.equals("⬅️ Назад")
				&& !data.equals("🔎 Посмотреть пример описания 🔎")) {

			setRentData.get(chatId)[dataId] = data;

		} else {
			return;
		}
		sendMessage.setText(nextStep);

		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void regApartamentStepPhoto(Message message, int dataId) {

		String chatId = message.getChatId().toString();
		List<PhotoSize> photos = message.getPhoto();

		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(message.getMessageId());

		File checkDir = new File("photos/get_rent/" + chatId); // проверка существования папки, если нет, то создать
		if (!checkDir.exists()) {
			System.out.println("no dir");
			checkDir.mkdirs();
		}

		String f_id = photos.stream().findFirst().orElse(null).getFileId();

		File photo = downloadPhotoByFilePath(getFilePath(photos.get(photos.size() - 2)));

		String fileName = f_id.substring(f_id.length() - 10, f_id.length());

		byte[] b = new byte[(int) photo.length()];
		File outputfile = new File("photos/get_rent/" + chatId + "/" + fileName + ".jpg");

		try (InputStream inputStream = new FileInputStream(photo);
				OutputStream outputStream = new FileOutputStream(outputfile)) {

			inputStream.read(b);
			outputStream.write(b);

		} catch (Exception e1) {
			e1.printStackTrace();
			sendExeptionToLog(message, e1);
		}

		setRentData.get(chatId)[dataId] += fileName + Constants.photosSplitMarker;

		String nextStep = "Нажмите кнопку \"Продолжить ➡️\" если данные верны, либо отправьте заново.";
		if (!message.hasPhoto() && !message.getText().equals("Продолжить ➡️")
				&& !message.getText().equals("🏡 Вернуться в главное меню 🏡")
				&& !message.getText().equals("⬅️ Вернуться к списку")) {

		} else {
			return;
		}
		sendMessage.setText(nextStep);

		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	public java.io.File downloadPhotoByFilePath(String filePath) {
		try {
			// Download the file calling AbsSender::downloadFile method
			System.out.println(filePath);

			return downloadFile(filePath);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String getFilePath(PhotoSize photo) {
		Objects.requireNonNull(photo);

		if (photo.hasFilePath()) { // If the file_path is already present, we are done!
			return photo.getFilePath();
		} else { // If not, let find it
			// We create a GetFile method and set the file_id from the photo
			GetFile getFileMethod = new GetFile();
			getFileMethod.setFileId(photo.getFileId());
			try {
				// We execute the method using AbsSender::execute method.
				org.telegram.telegrambots.api.objects.File file = execute(getFileMethod);
				// We now have the file_path
				return file.getFilePath();
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
		return null; // Just in case
	}

	private void preCheckOrder(Message message) {
		String preCheck = "";
		String[] data = setRentData.get(message.getChatId().toString());

		for (int i = 0; i < data.length; i++) {
			if (data[i] == null) {
				data[i] = "Нет данных.";
			}
		}

		String type = "";

		switch (data[1]) {
		case "room1":
			type = "1-комнатная";
			break;
		case "room2":
			type = "2-комнатная";

			break;
		case "room3":
			type = "3-комнатная";

			break;
		case "room4":
			type = "4-комнатная";

			break;
		case "roomOnly":
			type = "Комната";

			break;
		case "studio":
			type = "Студия";
			break;
		default:
			break;
		}

		preCheck += ("Район : " + data[0] + "\n");
		preCheck += ("Тип : " + type + "\n");
		preCheck += ("Станция метро : " + data[2] + "\n");
		preCheck += ("Адрес : " + data[3] + "\n");
		preCheck += ("Цена : " + data[5] + "\n");
		preCheck += ("Описание : " + data[4] + "\n");
		preCheckAdvertisements.put(message.getChatId().toString(), preCheck);

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(preCheck);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
			sendExeptionToLog(message, e);
		}
	}

	private void sendExeptionToLog(Message message, Exception e) {
		logger.sendExeptionToLog(e);
		keyboard = chooseKeyboard.startMenuKeyboard();
		try {
			execute(generalController.sendMsgNoReply(message, keyboard, Constants.happenException));
		} catch (TelegramApiException exc) {
			logger.sendExeptionToLog(exc);
		}
	}

}
