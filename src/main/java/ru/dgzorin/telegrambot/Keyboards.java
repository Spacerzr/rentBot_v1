package ru.dgzorin.telegrambot;

import java.util.ArrayList;

import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

public class Keyboards {

	private KeyboardButton kbHome = new KeyboardButton("🏡 Вернуться в главное меню 🏡");
	private KeyboardButton kbInfo = new KeyboardButton("☑️ Полезная информация ☑️");

	private KeyboardButton kbMoveToDistrictList = new KeyboardButton("⬅️ Выбрать другой район");

	private KeyboardButton kbRentSet = new KeyboardButton("🏘 Сдать");
	private KeyboardButton kbRentGet = new KeyboardButton("🏘 Снять");

	private KeyboardButton kbApartamentGet = new KeyboardButton("🏢 Снять квартиру");
	private KeyboardButton kbRoomGet = new KeyboardButton("🏢 Снять комнату");

	private KeyboardButton kbApartamentGetStudioRoom = new KeyboardButton("🏢 Студия");
	private KeyboardButton kbApartamentGetOneRoom = new KeyboardButton("🏢 1-комнатная");
	private KeyboardButton kbApartamentGetTwoRoom = new KeyboardButton("🏢 2-комнатная");
	private KeyboardButton kbApartamentGetThreeRoom = new KeyboardButton("🏢 3-комнатная");
	private KeyboardButton kbApartamentGetFourRoom = new KeyboardButton("🏢 4-комнатная");

	private KeyboardButton kbRentApartament = new KeyboardButton("✅ Снять эту квартиру");
	private KeyboardButton kbMoveToList = new KeyboardButton("⬅️ Вернуться к списку");

	private KeyboardButton kbStepContinue = new KeyboardButton("Продолжить ➡️");
	private KeyboardButton kbStepBack = new KeyboardButton("⬅️ Назад");

	private KeyboardButton kbStepToFinish = new KeyboardButton("✅ Отправить заявку ✅");
	private KeyboardButton kbChangeClientData = new KeyboardButton("🔃 Изменить");

	private KeyboardButton kbRulesAgree = new KeyboardButton("Принимаю ➡️");
	private KeyboardButton kbStepExample = new KeyboardButton("🔎 Посмотреть пример описания 🔎");

	private KeyboardButton kbApartamentSet = new KeyboardButton("🏢 Сдать квартиру");
	private KeyboardButton kbRoomSet = new KeyboardButton("🏢 Сдать комнату");

	private KeyboardButton kbSetAdvertisement = new KeyboardButton("✅ Разместить объявление ✅");

	private KeyboardButton kbAdminOrders = new KeyboardButton("Заявки на публикацию");
	private KeyboardButton kbAdminStartDelete = new KeyboardButton("Удаление из рабочей области");
	private KeyboardButton kbAdminNewPost = new KeyboardButton("✅ Опубликовать ✅");
	private KeyboardButton kbAdminEdit = new KeyboardButton("📝 Редактировать");
	private KeyboardButton kbAdminDelete = new KeyboardButton("❌ Удалить");

	private KeyboardButton kbAdminApartament = new KeyboardButton("🏢 Квартира");
	private KeyboardButton kbAdminRoom = new KeyboardButton("🏢 Комната");
	private KeyboardButton kbAdminDeleteThisPost = new KeyboardButton("❌ Удалить объявление");
	private KeyboardButton kbAdminEndEdit = new KeyboardButton("Сохранить изменения");
	private KeyboardButton kbAdminSpam = new KeyboardButton("Массовая рассылка");

	/*
	 * ============================================================================
	 * клавиатуры общие
	 * ============================================================================
	 */

	ArrayList<KeyboardRow> startMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		keyboardFirstRow.add(kbRentGet);
		keyboardFirstRow.add(kbRentSet);
		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbInfo);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> choiseDistrictMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbHome);
		keyboard.add(keyboardFirstRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentGetApartamentKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbApartamentGetOneRoom);
		keyboardFirstRow.add(kbApartamentGetTwoRoom);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbApartamentGetThreeRoom);
		keyboardSecondRow.add(kbApartamentGetFourRoom);

		KeyboardRow keyboardThirdRow = new KeyboardRow();
		keyboardThirdRow.add(kbApartamentGetStudioRoom);
		keyboardThirdRow.add(kbMoveToDistrictList);

		KeyboardRow keyboardFourthRow = new KeyboardRow();
		keyboardFourthRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);
		keyboard.add(keyboardFourthRow);

		return keyboard;
	}

	/*
	 * ============================================================================
	 * клавиатуры раздела "Снять"
	 * ============================================================================
	 */

	ArrayList<KeyboardRow> rentGetMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbApartamentGet);
		keyboardFirstRow.add(kbRoomGet);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentApartmentMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbMoveToList);
		keyboardFirstRow.add(kbRentApartament);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentGetStepNameKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbStepContinue);

		KeyboardRow keyboardSecondRow = new KeyboardRow();

		keyboardSecondRow.add(kbMoveToList);
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;

	}

	ArrayList<KeyboardRow> rentGetStepPhoneNumberKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbStepToFinish);

		KeyboardRow keyboardSecondRow = new KeyboardRow();

		keyboardSecondRow.add(kbMoveToList);
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentGetStepRegRequestKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbChangeClientData);
		keyboardFirstRow.add(kbStepToFinish);

		KeyboardRow keyboardSecondRow = new KeyboardRow();

		keyboardSecondRow.add(kbMoveToList);
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	/*
	 * ============================================================================
	 * клавиатуры раздела "Сдать"
	 * ============================================================================
	 */

	ArrayList<KeyboardRow> rentSetStepRulesKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbRulesAgree);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentSetMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbApartamentSet);
		keyboardFirstRow.add(kbRoomSet);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentApartamentStepKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbStepBack);
		keyboardFirstRow.add(kbStepContinue);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentApartamentStepWithExampleKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbStepBack);
		keyboardFirstRow.add(kbStepContinue);

		KeyboardRow keyboardSecondRow = new KeyboardRow();

		keyboardSecondRow.add(kbStepExample);

		KeyboardRow keyboardThirdRow = new KeyboardRow();

		keyboardThirdRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentApartamentStepNoBackKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbStepContinue);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentApartamentPreCheckDataKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbSetAdvertisement);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbStepBack);
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> rentSetStepNameKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbChangeClientData);
		keyboardFirstRow.add(kbStepContinue);

		KeyboardRow keyboardSecondRow = new KeyboardRow();

		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;

	}

	/*
	 * ============================================================================
	 * admin
	 * ============================================================================
	 */

	ArrayList<KeyboardRow> adminStartMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		keyboardFirstRow.add(kbAdminOrders);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbAdminStartDelete);
		KeyboardRow keyboardThirdRow = new KeyboardRow();

		keyboardThirdRow.add(kbAdminSpam);
		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> adminCRUDMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbAdminNewPost);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbAdminEdit);
		keyboardSecondRow.add(kbAdminDelete);

		KeyboardRow keyboardThirdRow = new KeyboardRow();

		keyboardThirdRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> adminDelStep1MenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbAdminApartament);
		keyboardFirstRow.add(kbAdminRoom);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> adminDeleteApartamentMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbMoveToList);
		keyboardFirstRow.add(kbAdminDeleteThisPost);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> adminEditMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		keyboardFirstRow.add(kbStepContinue);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbAdminEndEdit);

		KeyboardRow keyboardThirdRow = new KeyboardRow();
		keyboardThirdRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> adminEditEndMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbAdminEndEdit);

		KeyboardRow keyboardThirdRow = new KeyboardRow();
		keyboardThirdRow.add(kbHome);

		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}
}
