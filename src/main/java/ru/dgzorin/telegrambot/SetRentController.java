package ru.dgzorin.telegrambot;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.dgzorin.database.Database;

public class SetRentController {
	String getFinishOrder(String chatId, String[] data) {
		SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy 'at' HH.mm ':'");

		String order = "" + date.format(new Date()) + Constants.newOrderFromTelegram + "\nСДАТЬ";
		try {
			ResultSet rs = Database.getRenterDataByChatId(chatId);

			order += "\nАрендодатель : " + chatId + "\nИмя : " + rs.getString(1) + "\nТелефон : " + rs.getString(2)
					+ "\n";

			String type = "";

			switch (data[1]) {
			case "room1":
				type = "1-комнатная";
				break;
			case "room2":
				type = "2-комнатная";

				break;
			case "room3":
				type = "3-комнатная";

				break;
			case "room4":
				type = "4-комнатная";

				break;
			case "roomOnly":
				type = "Комната";

				break;
			case "studio":
				type = "Студия";
				break;
			default:
				break;
			}

			order += "\nПредлагаемая квартира :\nМетро " + data[2] + "\n" + data[3] + "\n" + data[0] + " район\n"
					+ "Тип : " + type + "\n" + data[5] + "\n" + data[4];

			order += "\nВоспользуйтесь административным ботом @arendavspb_admin_bot для публикации или редактирования заявки.";

		} catch (SQLException e) {
			e.printStackTrace();
			return "Error !";
		}
		return order;
	}
}
