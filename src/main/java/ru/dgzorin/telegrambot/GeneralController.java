package ru.dgzorin.telegrambot;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import ru.dgzorin.database.Database;
import ru.dgzorin.log.Logger;

public class GeneralController {

	private Logger logger = new Logger();

	SendMessage sendMsg(SendMessage sendMessage, Message message, String text) {

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(text);
		return sendMessage;
	}

	SendMessage sendMsg(SendMessage sendMessage, Message message, ArrayList<KeyboardRow> keyboard, String text) {

		sendMessage.enableMarkdown(true);

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(text);
		return sendMessage;
	}

	SendPhoto sendPhoto(Message message, ArrayList<KeyboardRow> keyboard, String fileName, String text) {

		SendPhoto sendPhotoRequest = new SendPhoto();
		File file = new File("photos/" + fileName + ".jpg");
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendPhotoRequest.setChatId(message.getChatId().toString());
		sendPhotoRequest.setNewPhoto(file);
		sendPhotoRequest.setCaption(text);
		sendPhotoRequest.setReplyMarkup(replyKeyboardMarkup);

		return sendPhotoRequest;
	}

	ArrayList<SendPhoto> sendDistrictList(SendMessage sendMessage, Message message) {

		ArrayList<String> districts = Database.getDistricts();
		ArrayList<SendPhoto> districtsList = new ArrayList<>();

		sendMessage.enableMarkdown(true);
		sendMessage.setChatId(message.getChatId().toString());

		for (String distr : districts) {

			SendPhoto sendPhotoRequest = new SendPhoto();
			File file;

			InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
			List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
			List<InlineKeyboardButton> rowInline = new ArrayList<>();
			if (distr == null || distr.equals("")) {
				file = new File("photos/missing_foto.jpg");
			} else {
				file = new File("photos/districts/" + distr + ".jpg");
			}

			rowInline.add(new InlineKeyboardButton().setText("Выбрать").setCallbackData(distr));

			rowsInline.add(rowInline);
			markupInline.setKeyboard(rowsInline);

			sendPhotoRequest.setChatId(message.getChatId().toString());

			sendPhotoRequest.setNewPhoto(file);
			sendPhotoRequest.setCaption(distr);
			sendPhotoRequest.setReplyMarkup(markupInline);

			districtsList.add(sendPhotoRequest);
		}
		return districtsList;
	}

	SendMessage sendRequestToAdmin(String text) {
		SendMessage sendMessage = new SendMessage();

		sendMessage.setChatId(Constants.adminChatId);
		sendMessage.setText(text);
		return sendMessage;
	}

	SendMessage sendMsgNoReply(Message message, ArrayList<KeyboardRow> keyboard, String text) {

		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setText(text);
		return sendMessage;
	}

	SendMessage sendMsgNoReply(Message message, String text) {

		SendMessage sendMessage = new SendMessage();
		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setText(text);
		return sendMessage;
	}

	List<SendMessage> sendMassMsgToClients(String text) throws SQLException {
		ResultSet rs = Database.getClientsChatId();
		List<SendMessage> list = new ArrayList<>();
		while (rs.next()) {
			SendMessage sendMessage = new SendMessage();
			sendMessage.setChatId(rs.getString(1));
			sendMessage.setText(text);
			list.add(sendMessage);
		}
		return list;
	}

	List<SendMessage> sendMassMsgToRenters(String text) throws SQLException {

		ResultSet rs = Database.getRentersChatId();
		List<SendMessage> list = new ArrayList<>();
		while (rs.next()) {
			SendMessage sendMessage = new SendMessage();
			sendMessage.setChatId(rs.getString(1));
			sendMessage.setText(text);
			list.add(sendMessage);
		}
		return list;
	}
}
