package ru.dgzorin.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ru.dgzorin.log.Logger;

public class Database {
	private static Connection connection;

	private static String dbName = "rentserivcedb";

	private static PreparedStatement ps;

	public synchronized static void connect() throws Exception {

		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection("JDBC:sqlite:" + dbName + ".db");

	}

	public synchronized static void disconnect() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	/*
	 * ============================================================================
	 * раздел "Снять"
	 * ============================================================================
	 */

	public static ArrayList<String> getDistricts() {
		ArrayList<String> districts = new ArrayList<>();
		try {
			ps = connection.prepareStatement("SELECT name FROM districts");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				districts.add(rs.getString(1));
			}
			return districts;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static boolean checkApartamentList(String tableName, String district) {
		try {
			ps = connection.prepareStatement("SELECT id FROM " + tableName
					+ " WHERE district = (SELECT id FROM districts WHERE name = '" + district + "');");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return false;
		}
	}

	public static ResultSet getApartamentList(String tableName, String district) {
		try {
			ps = connection.prepareStatement(
					"SELECT id, photoName, metroStation, address, price, (SELECT renterChatId FROM renters WHERE id = "
							+ tableName + ".renterId) as renterChatId FROM " + tableName
							+ " WHERE district = (SELECT id FROM districts WHERE name = '" + district + "');");

			ResultSet rs = ps.executeQuery();

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static ResultSet getSelectedApartamentPhotos(String tableName, String id) {
		try {
			ps = connection.prepareStatement(
					"SELECT photoName, renters.renterChatId FROM " + tableName + " INNER JOIN renters ON renters.id = "
							+ tableName + ".renterId WHERE " + tableName + ".id = '" + id + "';");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static ResultSet getSelectedApartamentData(String tableName, String id) {
		try {
			ps = connection.prepareStatement(
					"SELECT metroStation, address, price, (districts.name) as district, description FROM " + tableName
							+ " INNER JOIN districts ON districts.id =" + tableName + ".district WHERE " + tableName
							+ ".id ='" + id + "';");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static ResultSet getRenterData(String tableName, String id) {
		try {

			ps = connection.prepareStatement(
					"SELECT renterChatId, renterName, renterPhoneNumber FROM renters WHERE id = (SELECT renterId FROM "
							+ tableName + " WHERE id = '" + id + "');");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	/*
	 * ============================================================================
	 * работа с таблицей clients
	 * ============================================================================
	 */

	public static boolean getClientChatId(String chatId) {

		try {
			ps = connection.prepareStatement("SELECT * FROM clients WHERE chatId = '" + chatId + "';");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			Logger.sendStaticExeptionToLog(e);
			return false;

		}
	}

	public static void addNewClient(String chatId) {

		try {
			ps = connection.prepareStatement("INSERT INTO clients (chatId) VALUES ('" + chatId + "');");
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static void setClientPhoneNumber(String chatId, String phoneNumber) {
		try {
			ps = connection.prepareStatement(
					"UPDATE clients SET phoneNumber ='" + phoneNumber + "' WHERE chatId = '" + chatId + "';");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static String getClientPhoneNumber(String chatId) {
		try {
			ps = connection.prepareStatement("SELECT phoneNumber FROM clients WHERE chatId = '" + chatId + "';");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static void setClientName(String chatId, String clientName) {
		try {
			ps = connection
					.prepareStatement("UPDATE clients SET name ='" + clientName + "' WHERE chatId = '" + chatId + "';");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static String getClientName(String chatId) {
		try {
			ps = connection.prepareStatement("SELECT name FROM clients WHERE chatId = '" + chatId + "';");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}
	/*
	 * ============================================================================
	 * работа с таблицей renters
	 * ============================================================================
	 */

	public static boolean getRenterChatId(String chatId) {

		try {
			ps = connection.prepareStatement("SELECT * FROM renters WHERE renterChatId = '" + chatId + "';");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			Logger.sendStaticExeptionToLog(e);
			return false;
		}
	}

	public static void addNewRenter(String chatId) {

		try {
			ps = connection.prepareStatement("INSERT INTO renters (renterChatId) VALUES ('" + chatId + "');");
			ps.execute();

		} catch (SQLException e) {
			Logger.sendStaticExeptionToLog(e);
			e.printStackTrace();
		}
	}

	public static void setRenterPhoneNumber(String chatId, String phoneNumber) {
		try {
			ps = connection.prepareStatement("UPDATE renters SET renterPhoneNumber ='" + phoneNumber
					+ "' WHERE renterChatId = '" + chatId + "';");
			ps.execute();
		} catch (SQLException e) {
			Logger.sendStaticExeptionToLog(e);
			e.printStackTrace();
		}
	}

	public static String getRenterPhoneNumber(String chatId) {
		try {
			ps = connection
					.prepareStatement("SELECT renterPhoneNumber FROM renters WHERE renterChatId = '" + chatId + "';");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}
		} catch (SQLException e) {
			Logger.sendStaticExeptionToLog(e);
			e.printStackTrace();
			return null;
		}
	}

	public static void setRenterName(String chatId, String renterName) {
		try {
			ps = connection.prepareStatement(
					"UPDATE renters SET renterName ='" + renterName + "' WHERE renterChatId = '" + chatId + "';");
			ps.execute();
		} catch (SQLException e) {
			Logger.sendStaticExeptionToLog(e);
			e.printStackTrace();
		}
	}

	public static String getRenterName(String chatId) {
		try {
			ps = connection.prepareStatement("SELECT renterName FROM renters WHERE renterChatId = '" + chatId + "';");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}

		} catch (SQLException e) {
			Logger.sendStaticExeptionToLog(e);
			e.printStackTrace();
			return null;
		}
	}

	public static void postNewRequest(String chatId, String[] data) {

		try {
			ps = connection.prepareStatement("INSERT INTO requests"
					+ " (renterId, metroStation, address, district, description, price, photoName, tableName) "
					+ "VALUES ((SELECT id FROM renters WHERE renterChatId = '" + chatId + "'), '" + data[2] + "', '"
					+ data[3] + "', (SELECT id FROM districts WHERE name = '" + data[0] + "'), '" + data[4] + "', '"
					+ data[5] + "', '" + data[6] + "', '" + data[1] + "');");
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	/*
	 * ============================================================================
	 * admin
	 * ============================================================================
	 */

	public static boolean checkRequests() {
		try {
			ps = connection.prepareStatement("SELECT id FROM requests;");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return false;
		}
	}

	public static ResultSet getRequestsList() {
		try {
			ps = connection.prepareStatement(
					"SELECT requests.id, photoName, metroStation, address, price, (SELECT renterChatId FROM renters WHERE id = requests.renterId) as renterChatId, (districts.name) as district ,tableName FROM requests INNER JOIN districts ON districts.id = requests.district;");

			ResultSet rs = ps.executeQuery();

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static ResultSet getRenterData(String id) {
		try {
			ps = connection.prepareStatement(
					"SELECT renterName,renterPhoneNumber FROM renters WHERE id = (SELECT renterId FROM requests WHERE requests.id = '"
							+ id + "');");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static ResultSet getRenterDataByChatId(String chatId) {
		try {
			ps = connection.prepareStatement(
					"SELECT renterName,renterPhoneNumber FROM renters WHERE renterChatId ='" + chatId + "';");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static ResultSet getNewPostFromRequests(String id) {
		try {
			ps = connection.prepareStatement("SELECT * FROM requests WHERE id = '" + id + "';");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static void addNewPost(String id) {

		ResultSet rs = getNewPostFromRequests(id);

		try {
			ps = connection.prepareStatement("INSERT INTO " + rs.getString(9)
					+ " (renterId, metroStation, address, district, description, price, photoName) " + "VALUES ('"
					+ rs.getString(2) + "', '" + rs.getString(3) + "', '" + rs.getString(4) + "', '" + rs.getString(5)
					+ "', '" + rs.getString(6) + "', '" + rs.getString(7) + "', '" + rs.getString(8) + "');");
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static void removeRequest(String id) {

		try {
			ps = connection.prepareStatement("DELETE FROM requests WHERE id = '" + id + "';");
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static void removePost(String tableName, String id) {

		try {
			ps = connection.prepareStatement("DELETE FROM " + tableName + " WHERE id = '" + id + "';");
			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static String[] getEditData(String id) {
		String[] data = new String[9];
		try {
			ps = connection.prepareStatement("SELECT *  FROM requests WHERE id = '" + id + "';");
			ResultSet rs = ps.executeQuery();
			data[0] = rs.getString(1);
			data[1] = rs.getString(2);
			data[2] = rs.getString(3);
			data[3] = rs.getString(4);
			data[4] = rs.getString(5);
			data[5] = rs.getString(6);
			data[6] = rs.getString(7);
			data[7] = rs.getString(8);
			data[8] = rs.getString(9);

			return data;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static void setEditData(String[] data) {
		try {
			ps = connection.prepareStatement(
					"UPDATE requests SET metroStation ='" + data[2] + "', address = '" + data[3] + "', description = '"
							+ data[5] + "', price = '" + data[6] + "' WHERE id = '" + data[0] + "';");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
	}

	public static ResultSet getRentersChatId() {
		try {
			ps = connection.prepareStatement("SELECT renterChatId FROM renters;");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}

	public static ResultSet getClientsChatId() {
		try {
			ps = connection.prepareStatement("SELECT chatId FROM clients;");

			ResultSet rs = ps.executeQuery();
			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
			return null;
		}
	}
}
