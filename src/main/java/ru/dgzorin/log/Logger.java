package ru.dgzorin.log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH.mm ':'");

	private static SimpleDateFormat dateFormatStatic = new SimpleDateFormat("dd.MM.yy");
	private static SimpleDateFormat timeFormatStatic = new SimpleDateFormat("HH.mm ':'");

	public void sendExeptionToLog(Exception e) {

		checkDir();
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();

		String fileName = "logs/" + dateFormat.format(new Date()) + ".txt";
		File log = new File(fileName);
		if (!log.exists()) {
			try {
				log.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		FileWriter fr = null;
		try {
			fr = new FileWriter(log, true);
			fr.write(timeFormat.format(new Date()) + " " + exceptionAsString + "\r\n");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void sendStaticExeptionToLog(Exception e) {

		checkDirStatic();
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();

		String fileName = "logs/" + dateFormatStatic.format(new Date()) + ".txt";
		File log = new File(fileName);
		if (!log.exists()) {
			try {
				log.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		FileWriter fr = null;
		try {
			fr = new FileWriter(log, true);
			fr.write(timeFormatStatic.format(new Date()) + " " + exceptionAsString + "\r\n");

		} catch (IOException ex) {
			e.printStackTrace();
		} finally {
			try {
				fr.close();
			} catch (IOException ex) {
				e.printStackTrace();
			}
		}
	}

	private void checkDir() {
		File checkDir = new File("logs");
		if (!checkDir.exists()) {
			checkDir.mkdirs();
		}
	}

	private static void checkDirStatic() {
		File checkDir = new File("logs");
		if (!checkDir.exists()) {
			checkDir.mkdirs();
		}
	}

	public static void sendStaticTextToLog(String text) {

		checkDirStatic();

		String fileName = "logs/" + dateFormatStatic.format(new Date()) + ".txt";
		File log = new File(fileName);
		if (!log.exists()) {
			try {
				log.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		FileWriter fr = null;
		try {
			fr = new FileWriter(log, true);
			fr.write(timeFormatStatic.format(new Date()) + " " + text + "\r\n");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
