package ru.dgzorin.gui;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class App {
	public static final String APPLICATION_NAME = "Arenda v SPB Bot";
	public static final String ICON_STR = "icon.png";

	public static void createGUI() {
		JFrame frame = new JFrame(APPLICATION_NAME);
		frame.setMinimumSize(new Dimension(10, 10));
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.pack();
		setTrayIcon();
		StartWindow.closeStartWindow();
	}

	private static void setTrayIcon() {
		if (!SystemTray.isSupported()) {
			return;
		}
		PopupMenu trayMenu = new PopupMenu();
		MenuItem item = new MenuItem("Exit");
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		trayMenu.add(item);

		Image icon = Toolkit.getDefaultToolkit().getImage(ICON_STR);
		TrayIcon trayIcon = new TrayIcon(icon, APPLICATION_NAME, trayMenu);
		trayIcon.setImageAutoSize(true);

		SystemTray tray = SystemTray.getSystemTray();
		try {
			tray.add(trayIcon);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		trayIcon.displayMessage(APPLICATION_NAME, "Telegram bot started !", TrayIcon.MessageType.INFO);
	}

}
