package ru.dgzorin.gui;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class Main {
	public static final String APPLICATION_NAME = "Arenda v SPB Bot";
	private static JFrame frame = new JFrame(APPLICATION_NAME);

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createGUI();
			}
		});
	}

	private static void createGUI() {
		frame.setMinimumSize(new Dimension(300, 200));
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.pack();

		JPanel mainPanel = new JPanel(new FlowLayout());

		JLabel label = new JLabel("Выберите тип подключения.");

		final JRadioButton rb1 = new JRadioButton("Прямое подключение");
		final JRadioButton rb2 = new JRadioButton("Использовать прокси");
		ButtonGroup bg = new ButtonGroup();

		final JTextField ip = new JTextField(15);
		final JTextField port = new JTextField(5);

		JButton start = new JButton("Запустить бота");
		JButton linkProxyList = new JButton("Список доступных прокси");

		ip.setText("80.211.231.81");
		port.setText("3128");

		bg.add(rb1);
		bg.add(rb2);
		rb2.setSelected(true);

		mainPanel.add(label);
		mainPanel.add(rb1);
		mainPanel.add(rb2);
		mainPanel.add(ip);
		mainPanel.add(port);
		mainPanel.add(linkProxyList);
		mainPanel.add(start);

		frame.add(mainPanel);

		frame.setVisible(true);

		ip.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent e) {
				char a = e.getKeyChar();

				if (!Character.isDigit(a) && a != '.') {
					e.consume();
				}
			}
		});

		port.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent e) {
				char a = e.getKeyChar();
				if (!Character.isDigit(a)) {
					e.consume();
				}
			}
		});

		rb1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ip.setEnabled(false);
				port.setEnabled(false);
			}
		});

		rb2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ip.setEnabled(true);
				port.setEnabled(true);
			}
		});

		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (rb1.isSelected()) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							StartWindow.start();
						}
					}).start();
					frame.dispose();
				}
				if (rb2.isSelected()) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							int tmpPort = Integer.parseInt(port.getText());

							StartWindow.start(ip.getText(), tmpPort);
						}
					}).start();
					frame.dispose();
				}
			}
		});

		linkProxyList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					final URI uri = new URI("https://hidemy.name/ru/proxy-list/?type=h#list");
					open(uri);
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});

	}

	private static void open(URI uri) {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(uri);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
