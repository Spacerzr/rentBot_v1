package ru.dgzorin.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import ru.dgzorin.database.Database;
import ru.dgzorin.log.Logger;
import ru.dgzorin.telegrambot.AdminBot;
import ru.dgzorin.telegrambot.TelegramBot;

public class StartWindow {
	public static final String APPLICATION_NAME = "Arenda v SPB Bot";
	private static JFrame frame = new JFrame(APPLICATION_NAME);

	public static void start() {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createGUI();
			}
		});
		try {
			Database.connect();
			TelegramBot.startMainBot();
			AdminBot.startAdminBot();
		} catch (Exception e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
		Logger.sendStaticTextToLog("Telegram Bot Started !");
		App.createGUI();

	}

	public static void start(String ip, int port) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createGUI();
			}
		});
		try {
			Database.connect();
			TelegramBot.startMainBot(ip, port);
			AdminBot.startAdminBot(ip, port);
		} catch (Exception e) {
			e.printStackTrace();
			Logger.sendStaticExeptionToLog(e);
		}
		Logger.sendStaticTextToLog("Telegram Bot Started !");
		App.createGUI();

	}

	private static void createGUI() {
		frame.setMinimumSize(new Dimension(300, 80));
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.pack();

		JPanel mainPanel = new JPanel(new FlowLayout());

		JProgressBar progressBar1 = new JProgressBar();
		progressBar1.setIndeterminate(true);
		JLabel label = new JLabel("Пожалуйста, подождите, бот загружается.");

		mainPanel.add(label);
		mainPanel.add(progressBar1);
		frame.add(mainPanel);

		frame.setVisible(true);
	}

	public static void closeStartWindow() {
		frame.dispose();
	}

}
